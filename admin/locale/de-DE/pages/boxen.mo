��    P      �  k         �  
   �     �     �               ,     ?     Q     f     u     �     �  
   �     �  
   �     �  
   �     �     �               +     7  	   E     O     [     h  	   x     �     �     �     �     �     �     �     �  	   �     �  	   �     �     	     	     )	     8	     G	     V	     c	     |	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	     
     
     
     )
  
   6
     A
     P
     ^
     o
     �
     �
     �
     �
     �
     �
     �
               4     F     Y     s  (   �  
   �     �     �     �               (     :     O     ^     v     �  
   �     �  
   �     �  
   �  	   �     �     �           /     ;  	   I     S     _     l  	   |     �     �     �     �     �     �     �     �  ;  �          7  �   K  �   �     �     �  %   �  !   �  #     ~   :  '   �     �     �  
                  (  7   F  8   ~     �     �     �     �     �     
          1  #   F  (   j  !   �  &   �  %   �  	               	        $     +     3  !   M  �   o  @   �     =              H       !      $   "   5      J       I       O   '   G       6   8      L      K   :             F      C      2          ,   ?   @          M      (       3   B   A      0   >          +       .         P      1                  )         D              #             9                        *       -               N   E      4   7   /                  	   &   <   ;   %   
    Bestseller Eigene Box (Mit Rahmen) Eigene Box (Ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Suchspecial) Filter (Tag) Globale Merkmale Hersteller In Kürze Verfügbar Kategorien Konfigurator Linkgruppe Login Neue im Sortiment News (Monatsübersicht) News Kategorien Schnellkauf Sonderangebot Suchwolke Top Angebot Top bewertet Vergleichsliste Warenkorb Wunschliste Zuletzt angesehen boxEdit boxLabel boxTemplate boxTitle boxType boxen boxenDesc boxenURL catBoxNum catBoxNumTooltip confirmDeleteBox deactivatedOnAllPages deleteSelected errorBoxCreate errorBoxDelete errorBoxEdit errorBoxesVisibilityEdit errorContainerCreate inContainer invisibleBoxes linkgroup new newContainer noBoxActivated noBoxesAvailableFor noTemplateConfig pleaseSelect remove sectionBottom sectionLeft sectionRight sectionTop showBoxOnlyFor showContainer successBoxCreate successBoxDelete successBoxEdit successBoxRefresh successContainerCreate templateTypeCategory templateTypeContent templateTypeExtension templateTypeLinkList templateTypePlugin templateTypeTemplate visibleOnAllPages visibleOnSomePages warningChangesForAllPages warningInvisibleBoxes Content-Type: text/plain; charset=UTF-8
 Bestseller Eigene Box (mit Rahmen) Eigene Box (ohne Rahmen) Filter (Bewertung) Filter (Hersteller) Filter (Kategorie) Filter (Merkmale) Filter (Preisspanne) Filter (Suche) Filter (Artikelsticker) Filter (Tag) Globale Merkmale Hersteller In Kürze verfügbar Kategorien Konfigurator Linkgruppe Anmeldung Neu im Sortiment Blog: Monate mit Beiträgen Blog: Kategorien des Blogsystems Schnellkauf Sonderangebot Suchwolke Top-Artikel Top bewertet Vergleichsliste Warenkorb Wunschzettel Zuletzt angesehen Box bearbeiten Bezeichnung Template Titel Typ Footer / Boxen Hier können Sie Boxen in verschiedenen Boxen-Containern verwalten und den Footer pflegen. Für jeden Container können Sie festlegen, auf welchen Seiten er sichtbar sein soll. Je nach Template sind verschiedene Container verfügbar. Einige Boxen können Sie über die Bearbeiten-Schaltfläche weiter konfigurieren. https://jtl-url.de/2uknw Kategoriebox-Nummer Listet nur die Kategorien mit dem Kategorieattribut „kategoriebox“ aus JTL-Wawi und der gesetzten Nummer auf. Standard=0 für alle Kategorien mit oder ohne Funktionsattribut. Sind Sie sicher, dass Sie die Box „%s“ löschen möchten? Wenn Sie einen Container löschen, werden alle im Container enthaltenen Boxen ebenfalls entfernt. auf allen Seiten deaktiviert Markierte löschen Box konnte nicht hinzugefügt werden. Box konnte nicht entfernt werden. Box konnte nicht bearbeitet werden. Sichtbarkeit der Boxen konnte nicht gesetzt werden. Es ist ein MySQL-Fehler aufgetreten. Bitte wenden Sie sich an den Support. Container konnte nicht angelegt werden. In Container Nicht sichtbare Boxen Linkgruppe Neue Box Neuer Container Es wurde keine Box aktiviert. Momentan sind noch keine Boxen für „%s“ vorhanden. Konfigurationsdatei (template.xml) wurde nicht gefunden. Bitte auswählen Aus Liste entfernen Footer Linke Seitenleiste Rechte Seitenleiste Header Box „%s“ nur anzeigen für: Container aktivieren Box wurde erfolgreich hinzugefügt.  Box(en) wurde(n) erfolgreich gelöscht. Box wurde erfolgreich bearbeitet. Boxen wurden erfolgreich aktualisiert. Container wurde erfolgreich angelegt. Kategorie Inhalt Plugin Linkliste Plugin Vorlage sichtbar auf allen Seiten sichtbar auf ausgewählten Seiten Die Auswahl „Alle Seiten“ überschreibt entsprechende Änderungen an den Boxen, die vorher für einzelne Seiten vorgenommen wurden. Es existieren Boxen, die aktuell nicht angezeigt werden können. 